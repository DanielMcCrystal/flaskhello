FROM python:3-alpine
COPY dist/* /dist/
RUN python3 -m pip install gunicorn /dist/*.whl && rm -r /dist
RUN apk add binutils libc-dev
CMD gunicorn -b :4000 flaskhello:app
EXPOSE 4000
